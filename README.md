# Medical Marijuana Reciprocity Guide
A quick reference map on which Medical Marijuana states accept cards from other 
states

[https://kaitiggy.gitlab.io/mmj-reciprocity/](https://kaitiggy.gitlab.io/mmj-reciprocity/)

![image preview](https://i.imgur.com/Xvp1ahY.png)

> *Disclaimer: For reference purposes only, not to be considered legal advice.* 
*I am not a lawyer, just a stoner programmer.*